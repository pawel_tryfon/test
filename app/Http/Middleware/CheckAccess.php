<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ...$roles)
    {
        $allow = false;
        $user = $request->user();
        if(isset($user)) {
          foreach ($roles as $role) {
              switch ($role) {
                case 'admin':
                  $allow = $user->isAdmin();
                  break;
                case 'security':
                  $allow = $user->isSecurity();
                  break;
              }
              if($allow) {
                return $next($request);
              }
          }
        }
        return redirect('home');

    }
}
