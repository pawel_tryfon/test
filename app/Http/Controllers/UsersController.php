<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Work;
use App\Models\Section;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $search = $request->all();
      $s = isset($search['search'])?$search['search']:'';
      $users = User::where('name', 'LIKE', '%'.$s.'%');

      if(isset($search['work'])) {
        $users->where('work_id', $search['work']);
      }

      if(isset($search['section'])) {
        $users->where('section_id', $search['section']);
      }

      $users = $users->paginate(15);


      return view('users.index', compact('users', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $works = Work::all();
      $sections = Section::all();
         return view('users.create', compact('works', 'sections'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
      'name' => 'required|min:3|max:50',
      'email' => 'required|email',
      'password' => 'min:6|required_with:password2|same:password2',
      'password2' => 'min:6',
      'work_id' => 'nullable|exists:works,id',
      'section_id' => 'nullable|exists:sections,id',
      ]);

     $data = $request->all();
     if(isset($data['password'])) {
       $data['password'] = Hash::make($data['password']);
     }

      $roles = 0;
      if(isset($data['roles'])) {
        foreach ($data['roles'] as $role => $tmp) {
          $roles |= intval($role);
        }
      }
      $data['roles'] = $roles;

     User::create($data);

     if($request->ajax()){
         exit;
     }

     return redirect()->route('users.index')
                     ->with('success','Zapisano.');
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
      $works = Work::all();
      $sections = Section::all();
      return view('users.edit',compact('user', 'works', 'sections'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
      $this->validate($request, [
      'name' => 'required|min:3|max:50',
      'email' => 'required|email',
      'password' => 'same:password2',
      'work_id' => 'nullable|exists:works,id',
      'section_id' => 'nullable|exists:sections,id',
      ]);

     $data = $request->all();

     if(isset($data['password'])) {
       $data['password'] = Hash::make($data['password']);
     } else {
       unset($data['password']);
     }

      $roles = 0;
      if(isset($data['roles'])) {
        foreach ($data['roles'] as $role => $tmp) {
          $roles |= intval($role);
        }
      }
      $data['roles'] = $roles;

      // var_dump($data);exit;

      $user->update($data);

      if($request->ajax()){
          exit;
      }

      return redirect()->route('users.index')
                      ->with('success','Zapisano');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
      $user->delete();

     return redirect()->route('users.index')
                     ->with('success','Usunięto');
    }
}
