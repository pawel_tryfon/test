<?php

namespace App\Http\Controllers;

use App\Models\Worker;
use App\Models\Work;
use App\Models\Section;
use App\Models\WorkerLog;
use Illuminate\Http\Request;

class WorkersLogsController extends Controller
{

  public function __construct() {
    $this->middleware('checkAccess:admin,security');
  }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $user = \Auth::user();
      $where = [];
      $name = '';
      $worker = null;

      $search = $request->all();

      if(isset($request->worker)) {
        $worker = Worker::find($request->worker);
        if(isset($worker)) {
          $where['worker_id'] = $worker->id;
          $name = $worker->name .' '. $worker->surname;
        }
      }

      if(isset($request->work)) {
        $where['work_id'] = $request->work;

        if(isset($user->section)) {
          $where['section_id'] = $request->section;
        }
      }

      if(isset($user->work)) {
        $where['work_id'] = $user->work->id;
        if(isset($user->section)) {
          $where['section_id'] = $user->section->id;
        }
      }elseif(!$user->isAdmin()) {
        $where['worker_id'] = -1;
      }

      if(empty($name)) {
        if(isset($where['work_id'])) {
          $work = Section::find($where['work_id']);
          $name = $work->name;
        }
        if(isset($where['section_id'])) {
          $section = Section::find($where['section_id']);
          $name .= ' / '.$section->name;
        }
      }


      $logs = WorkerLog::where($where)->orderBy('id', 'DESC')->paginate(15);

      return view('workers_logs.index', compact('worker', 'logs', 'name', 'search'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
      $worker = Worker::find($request->worker);
        return view('workers_logs.create',compact('worker'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $user = \Auth::user();
      $worker = Worker::find($request->worker);
      $this->validate($request, [
        'start' => 'required|date|after:yesterday|before:now'
// 'finish_date' => 'required|date|after:start_date'
      ]);

     $data = $request->all();
     $data['user_id'] = $user->id;
     $data['worker_id'] = $worker->id;
     $data['work_id'] = $worker->work->id;
     $data['section_id'] = $worker->section->id;

     WorkerLog::create($data);

     if($request->ajax()){
         exit;
     }

     return redirect()->route('workers_logs.index', ['worker'=>$worker->id])
                     ->with('success','Zapisano.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\WorkerLog  $workerLog
     * @return \Illuminate\Http\Response
     */
    public function show(WorkerLog $workerLog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\WorkerLog  $workerLog
     * @return \Illuminate\Http\Response
     */
    public function edit(WorkerLog $workerLog)
    {
      // dd($workerLog);
        return view('workers_logs.edit',compact('workerLog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\WorkerLog  $workerLog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WorkerLog $workerLog)
    {
      $user = \Auth::user();


      $this->validate($request, [
        'start' => 'required|date|after:yesterday|before:now',
        'stop' => 'required|date|after:start|before:now',//.date('Y-m-d H:i:s')
      ]);
      $data = $request->all();
      $data['user_id'] = $user->id;

      $workerLog->update($data);
      if($request->ajax()){
          exit;
      }

      return redirect()->route('workers_logs.index', ['worker'=>$workerLog->worker->id])
                      ->with('success','Zapisano');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\WorkerLog  $workerLog
     * @return \Illuminate\Http\Response
     */
    public function destroy(WorkerLog $workerLog)
    {
      // dd($workerLog->worker);
      // $workerId = $workerLog->worker->id;
      $workerLog->delete();

     return redirect()->route('workers_logs.index', ['worker'=>1])
                     ->with('success','Usunięto');
    }


    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function export(Request $request)
    {


       $user = \Auth::user();
       $where = [];
       $name = '';
       $worker = null;

       if(isset($request->worker)) {
         $worker = Worker::find($request->worker);
         if(isset($worker)) {
           $where['worker_id'] = $worker->id;
           $name = $worker->name .' '. $worker->surname;
         }
       }

       if(isset($request->work)) {
         $where['work_id'] = $request->work;

         if(isset($request->section)) {
           $where['section_id'] = $request->section;
         }
       }

       if(isset($user->work)) {
         $where['work_id'] = $user->work->id;
         if(isset($user->section)) {
           $where['section_id'] = $user->section->id;
         }
       }elseif(!$user->isAdmin()) {
         $where['worker_id'] = -1;
       }

       if(empty($name)) {
         if(isset($where['work_id'])) {
           $work = Work::find($where['work_id']);
           $name = $work->name;
         }
         if(isset($where['section_id'])) {
           $section = Section::find($where['section_id']);
           $name .= ' - '.$section->name;
         }
       }
       if(!empty($name)) {
         $name = '['.$name.']-';
       }
       $fileName = $name. 'raport-czasu-pracy-'.date('Y-m-dTH:i:s').'.csv';
       $headers = array(
           "Content-type"        => "text/csv",
           "Content-Disposition" => "attachment; filename=$fileName",
           "Pragma"              => "no-cache",
           "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
           "Expires"             => "0"
       );


       $logs = WorkerLog::where($where)->orderBy('id', 'DESC')->get();

       // dd($where);


        $columns = array('Nr', 'Pracownik', 'Miejsce', 'Wejście', 'Wyjście');

        $callback = function() use($logs, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            $i = 1;
            foreach ($logs as $log) {
                $row['Nr']  = $i++;
                $row['Pracownik']  = isset($log->worker)?$log->worker->name .' ' .$log->worker->surname:'';
                $row['Miejsce'] = '';
                if(isset($log->work)) {
                  $row['Miejsce'] = $log->work->name;
                   if(isset($log->section)) {
                     $row['Miejsce'] .= '/'. $log->section->name;
                   }
                 }
                $row['Wejście']  = $log->start;
                $row['Wyjście']  = $log->stop;

                fputcsv($file, $row);

            }
            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }
}
