<?php

namespace App\Http\Controllers;

use App\Models\Worker;
use App\Models\Work;
use App\Models\Section;
use Illuminate\Http\Request;

class WorkersController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
      $search = $request->all();
      $s = isset($search['search'])?$search['search']:'';
      $workers = Worker::where('name', 'LIKE', '%'.$s.'%');

      if(!\Auth::user()->isAdmin()) {
        if(isset(\Auth::user()->work_id)) {
          $search['work'] = \Auth::user()->work_id;
          $search['section'] = \Auth::user()->section_id;
        } else {
          $search['work'] = -1;
        }
      }

      if(isset($search['work'])) {
        $workers->where('work_id', $search['work']);
      }

      if(isset($search['section'])) {
        $workers->where('section_id', $search['section']);
      }

      $workers = $workers->paginate(15);


      return view('workers.index', compact('workers', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $works = Work::all();
      $sections = Section::all();
        return view('workers.create',compact('works', 'sections'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
      'name' => 'required|min:3|max:50',
      'surname' => 'required|min:3|max:50',
      'work_id' => 'nullable|exists:works,id',
      'section_id' => 'nullable|exists:sections,id',

      ]);

     $data = $request->all();
     Worker::create($data);

     if($request->ajax()){
         exit;
     }

     return redirect()->route('workers.index')
                     ->with('success','Zapisano.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Worker  $worker
     * @return \Illuminate\Http\Response
     */
    public function show(Worker $worker)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Worker  $worker
     * @return \Illuminate\Http\Response
     */
    public function edit(Worker $worker)
    {
      $works = Work::all();
      $sections = Section::all();
        return view('workers.edit',compact('worker', 'works', 'sections'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Worker  $worker
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Worker $worker)
    {
      $this->validate($request, [
      'name' => 'required|min:3|max:50',
      'surname' => 'required|min:3|max:50',
      'work_id' => 'nullable|exists:works,id',
      'section_id' => 'nullable|exists:sections,id',
      ]);

     $data = $request->all();

      $worker->update($data);

      if($request->ajax()){
          exit;
      }

      return redirect()->route('workers.index')
                      ->with('success','Zapisano');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Worker  $worker
     * @return \Illuminate\Http\Response
     */
    public function destroy(Worker $worker)
    {
      $worker->delete();

     return redirect()->route('workers.index')
                     ->with('success','Usunięto');
    }
}
