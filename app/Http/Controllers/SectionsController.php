<?php

namespace App\Http\Controllers;

use App\Models\Section;
use App\Models\Work;
use Illuminate\Http\Request;

class SectionsController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
      $work = Work::find($request->work);
      $sections = Section::where('work_id', $request->work)
        ->paginate(15);

      return view('sections.index', compact('sections', 'work'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $work = Work::find($request->work);
        return view('sections.create', compact('work'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
      'name' => 'required|min:3|max:50',

      ]);

     $data = $request->all();
     if(isset($request->work)) {
       $data['work_id'] = $request->work;
     }
     Section::create($data);

     if($request->ajax()){
         exit;
     }

     return redirect()->route('sections.index')
                     ->with('success','Zapisano.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function show(Section $section)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function edit(Section $section)
    {
        return view('sections.edit',compact('section'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Section $section)
    {
      $this->validate($request, [
      'name' => 'required|min:3|max:50',

      ]);

     $data = $request->all();

      $section->update($data);

      if($request->ajax()){
          exit;
      }

      return redirect()->route('sections.index')
                      ->with('success','Zapisano');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function destroy(Section $section)
    {
      $section->delete();

     return redirect()->route('sections.index')
                     ->with('success','Usunięto');
    }
}
