<?php

namespace App\Http\Controllers;

use App\Models\Work;
use Illuminate\Http\Request;

class WorksController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
    {
      $search = $request->all();
      $s = isset($search['search'])?$search['search']:'';
      $works = Work::where('name', 'LIKE', '%'.$s.'%')
        ->paginate(15);


      return view('works.index', compact('works', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('works.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
      'name' => 'required|min:3|max:50',

      ]);

     $data = $request->all();
     Work::create($data);

     if($request->ajax()){
         exit;
     }

     return redirect()->route('works.index')
                     ->with('success','Zapisano.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Work  $work
     * @return \Illuminate\Http\Response
     */
    public function show(Work $work)
    {
        //
    }

    /**
    * Display the specified resource.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
   public function sections(Request $request)
   {
     $work = Work::find($request->work);
     $response = array(
          'status' => 'success',
          'sections' => $work->sections
      );

      return response()->json($response);
   }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Work  $work
     * @return \Illuminate\Http\Response
     */
    public function edit(Work $work)
    {
        return view('works.edit',compact('work'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Work  $work
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Work $work)
    {
      $this->validate($request, [
      'name' => 'required|min:3|max:50',

      ]);

     $data = $request->all();

      $work->update($data);

      if($request->ajax()){
          exit;
      }

      return redirect()->route('works.index')
                      ->with('success','Zapisano');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Work  $work
     * @return \Illuminate\Http\Response
     */
    public function destroy(Work $work)
    {
      $work->delete();

     return redirect()->route('works.index')
                     ->with('success','Usunięto');
    }
}
