<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Work;
use App\Models\Section;

class Section extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',  'work_id', 
    ];


    public function work()
    {
        return $this->belongsTo(Work::class,'work_id');
    }

}
