<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\User;
use App\Models\Worker;
use App\Models\Work;
use App\Models\Section;


class WorkerLog extends Model
{
    use HasFactory;

    public $table = 'workers_logs';

    protected $fillable = [
        'start', 'stop', 'worker_id', 'work_id', 'section_id', 'user_id'
    ];


    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function worker()
    {
        return $this->belongsTo(Worker::class,'worker_id');
    }

    public function work()
    {
        return $this->belongsTo(Work::class,'work_id');
    }

    public function section()
    {
        return $this->belongsTo(Section::class,'section_id');
    }
}
