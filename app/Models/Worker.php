<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Work;
use App\Models\Section;

class Worker extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'surname', 'work_id', 'section_id'
    ];


    public function work()
    {
        return $this->belongsTo(Work::class,'work_id');
    }

    public function section()
    {
        return $this->belongsTo(Section::class,'section_id');
    }
}
