<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
// Route::get('/users/list', [App\Http\Controllers\UsersController::class, 'list'])->name('users.list');


Route::resource('users', App\Http\Controllers\UsersController::class)->middleware('checkAccess:admin');
Route::resource('workers', App\Http\Controllers\WorkersController::class)->middleware('checkAccess:admin,security');
Route::resource('works', App\Http\Controllers\WorksController::class)->middleware('checkAccess:admin');
Route::resource('sections', App\Http\Controllers\SectionsController::class)->middleware('checkAccess:admin');
// Route::resource('workers_logs', App\Http\Controllers\WorkersLogsController::class)->middleware('checkAccess:admin,security');
Route::post('/works/sections', [App\Http\Controllers\WorksController::class, 'sections'])->name('works.sections');

Route::get('workers_logs/export', [App\Http\Controllers\WorkersLogsController::class,'export'])->name('workers_logs.export');
Route::get('workers_logs', [App\Http\Controllers\WorkersLogsController::class,'index'])->name('workers_logs.index');
Route::post('workers_logs', [App\Http\Controllers\WorkersLogsController::class,'store'])->name('workers_logs.store');
Route::get('workers_logs/create', [App\Http\Controllers\WorkersLogsController::class,'create'])->name('workers_logs.create');
Route::get('workers_logs/{worker_log}', [App\Http\Controllers\WorkersLogsController::class,'show'])->name('workers_logs.show');
Route::put('workers_logs/{worker_log}', [App\Http\Controllers\WorkersLogsController::class,'update'])->name('workers_logs.update');
Route::delete('workers_logs/{worker_log}', [App\Http\Controllers\WorkersLogsController::class,'destroy'])->name('workers_logs.destroy');
Route::get('workers_logs/{worker_log}/edit', [App\Http\Controllers\WorkersLogsController::class,'edit'])->name('workers_logs.edit');
