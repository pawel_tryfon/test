@extends('layouts.app')
@section('content')

<h1>{{ $work->name }}</h1>
<h2>Lista działów</h2>
<div class="row">
  <div class="col-lg-12 actions">
    <div class="form-group">
      @if(isset($work))
        <a class="modal-action btn btn-primary" href="{{ route('sections.create', ['work'=>$work->id]) }}">Dodaj</a>
      @endif
    </div>
  </div>
<table class="table table-bordered table-responsive-lg">
       <tr>
           <th>Nazwa</th>
           <th>Akcje</th>

       </tr>
       @foreach ($sections as $section)
           <tr>
               <td>{{ $section->name }}</td>
               <td>
                 <a class="btn btn-primary" href="{{ route('workers_logs.export', ['work'=>$work->id, 'section'=>$section->id]) }}">Wejścia/wyjścia</a>
                 <a class="modal-action btn btn-primary" href="{{ route('sections.edit',$section->id) }}">Edytuj</a>

                 <form action="{{ route('sections.destroy',$section->id) }}" method="POST">
                     @csrf
                     @method('DELETE')
                     <button type="submit" class="btn btn-danger">Usuń</button>
                 </form>
               </td>
           </tr>
       @endforeach
   </table>

   {!! $sections->links() !!}

   @include('dialog')

@endsection
