<script type="application/javascript">
$(document).ready(function(){

  function formSubmit($form) {
    var method = $form.attr('method');
    var url = $form.attr('action');
    var datas = $form.serialize();
    if($form.find('[name="_method"]').length) {
      method = $form.find('[name="_method"]').val();
    }
    $.ajax({
      method: method,
      url: url,
      data: datas
    }).done(function( data ) {
      console.log(data);
      if (!data || /^\s*$/.test(data)) {
        window.location = window.location.href;
      } else {
        var $form = $(data).find('main form');
        $('#dialog .modal-body').empty().append($form);
        $form.submit(function(){
          formSubmit($(this));
          return false;
        });
        $('#dialog .modal-footer .save').unbind('click').click(function(){
          $form.submit();
        });
      }
    }).fail(function( jqXHR, textStatus, errorThrown ) {
      if(jqXHR.responseJSON != undefined) {
        if(jqXHR.responseJSON.errors != undefined) {
          $form.find('.errors').remove();
          $.each(jqXHR.responseJSON.errors,function(k, v){
            console.log(k, v);
            var $fg = $form.find('[name="'+k+'"]').parent();
            console.log($fg);
            // if(!$fg.find('.errors').length) {
              $fg.append('<ul class="errors"></ul>');
            // }
            var $errs = $fg.find('.errors');
            $errs.empty();
            $(v).each(function(i, item){
              $errs.append('<li>'+item+'</li>');
            });
          });
        }
      }
    });
  }

  $('.modal-action').click(function(){
    var $this = $(this);
    $('#dialog .modal-title').html($this.html());
    $.get($this.attr('href'), function(data){
      var $form = $(data).find('main form');
      $form.find('[type="submit"]').hide();
      $form.submit(function(){
        formSubmit($(this));
        return false;
      });
      $('#dialog .modal-body').empty().append($form);
      $('#dialog .modal-footer .save').unbind('click').click(function(){
        $form.submit();
      });
    });
    $('#dialog').modal('show');
    return false;
  });
});
</script>
   <div class="modal fade" id="dialog" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Zamknij</button>
        <button type="button" class="btn btn-primary save">Zapisz</button>
      </div>
    </div>
  </div>
</div>
