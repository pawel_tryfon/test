@extends('layouts.app')
@section('content')
<h1>Firma ochroniarska</h1>

<ul class="list-group">
@guest
    @if (Route::has('login'))
        <li class="list-group-item">
            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
        </li>
    @endif


@else
@if(Auth::user()->isAdmin())
<li class="list-group-item">
    <a class="nav-link" href="{{ route('works.index') }}">Zakłady</a>
</li>
<li class="list-group-item">
    <a class="nav-link" href="{{ route('users.index') }}">Użytkownicy</a>
</li>
@endif
@if(Auth::user()->isAdmin() || Auth::user()->isSecurity())
<li class="list-group-item">
    <a class="nav-link" href="{{ route('workers.index') }}">Pracownicy</a>
</li>
@endif

@endguest
</ul>

@endsection
