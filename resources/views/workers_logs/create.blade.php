@extends('layouts.app')
@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
          <h2>{{ $worker->name }} {{ $worker->surname }}</h2>
          <h3>Historia pracownika</h3>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('workers_logs.index', ['worker'=>$worker->id]) }}"> Wróć</a>
        </div>
    </div>
</div>

@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Uwaga!</strong> Wystąpiły błędy.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{ route('workers_logs.store', ['worker'=>$worker->id]) }}" method="POST">
    @csrf

     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Wejście:</strong>
                <input type="datetime-local" name="start" id="start" value="{{ old('start') ? old('start') : date('Y-m-d\TH:i:s') }}" class="form-control">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
          <button type="submit" class="btn btn-primary">Zapisz</button>
        </div>
    </div>

</form>

<!-- <script type="application/javascript">
function p(number){return number.toString().padStart(2, '0');}//number to 2 digit, 0 padded string
  var t = new Date();
  document.getElementById("start").defaultValue = `${t.getFullYear()}-${p(t.getMonth()+1)}-${p(t.getDate())}T${p(t.getHours())}:${p(t.getMinutes())}`;;
</script> -->

@endsection
