@extends('layouts.app')
@section('content')

<h2>{{ $name }}</h2>
<h3>Historia wejść i wyjść</h3>
<div class="row">
  <div class="col-lg-12 actions">
    <div class="form-group">
      <a class="btn btn-primary" href="{{ route('workers.index') }}">Wróć</a>
      @if(isset($search))
      <a class="btn btn-primary" href="{{ route('workers_logs.export', $search) }}">Raport</a>
      @endif
      @if(isset($worker))
      <a class="modal-action btn btn-success float-right" href="{{ route('workers_logs.create',['worker'=>$worker->id]) }}">Nowe wejście</a>
      @endif
    </div>
  </div>
<table class="table table-bordered table-responsive-lg">
       <tr>
           <th>Miejsce</th>
           <th>Wejście</th>
           <th>Wyjście</th>
           <th>Akcje</th>

       </tr>
       @foreach ($logs as $log)
           <tr>
               <td>
                 @if(isset($log->work))
                  {{ $log->work->name }}
                    @if(isset($log->section))
                    / {{ $log->section->name }}
                    @endif
                 @endif
               </td>
               <td>{{ $log->start }}</td>
               <td>{{ $log->stop }}</td>
               <td>
                 <a class="modal-action btn btn-primary" href="{{ route('workers_logs.edit',$log->id) }}">Ustaw</a>
                 <form action="{{ route('workers_logs.destroy',$log->id) }}" method="POST">
                     @csrf
                     @method('DELETE')
                     <button type="submit" class="btn btn-danger">Usuń</button>
                 </form>
               </td>
           </tr>
       @endforeach
   </table>




   {!! $logs->links() !!}

   @include('dialog')

@endsection
