@extends('layouts.app')
@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
<h2>{{ $workerLog->worker->name }} {{ $workerLog->worker->surname }}</h2>
          <h3>Historia pracownika</h3>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('workers.index') }}"> Wróć</a>
        </div>
    </div>
</div>

@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Uwaga!</strong> Wystąpiły błędy.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{ route('workers_logs.update',$workerLog->id) }}" method="POST">
    @csrf
    @method('PUT')

     <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Wejście:</strong>
                <input type="datetime-local" name="start" id="start" value="{{ date('Y-m-d\TH:i:s', strtotime($workerLog->start)) }}" class="form-control">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Wyjście:</strong>
                <input type="datetime-local" name="stop" id="stop" value="{{ isset($workerLog->stop)?date('Y-m-d\TH:i:s', strtotime($workerLog->stop)):'' }}" class="form-control">
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
          <button type="submit" class="btn btn-primary">Zapisz</button>
        </div>
    </div>

</form>


<!-- <script type="application/javascript">
function p(number){return number.toString().padStart(2, '0');}//number to 2 digit, 0 padded string
  var t = new Date();
  var el = document.getElementById("start")
  if(el.defaultValue.trim()=="") {
    el.defaultValue = `${t.getFullYear()}-${p(t.getMonth()+1)}-${p(t.getDate())}T${p(t.getHours())}:${p(t.getMinutes())}`;;
  }
</script> -->

@endsection
