@extends('layouts.app')
@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Nowy użytkownik</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('users.index') }}"> Wróć</a>
        </div>
    </div>
</div>

@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Uwaga!</strong> Wystąpiły błędy.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{ route('users.store') }}" method="POST">
    @csrf

     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Imię i nazwisko:</strong>
                <input type="text" name="name" class="form-control" placeholder="Podaj imię i nazwisko"  value="{{ old('name') }}">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Email:</strong>
                <input class="form-control" name="email" type="email" placeholder="Email" value="{{ old('email') }}"/>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="btn-group btn-group-toggle" data-toggle="buttons">
              <?php $roles = old('roles');?>
              @foreach(array(App\Models\User::ROLE_SECURITY => 'ochroniarz', App\Models\User::ROLE_ADMIN => 'administrator') as $role => $label)
                <label class="btn btn-light btn-sm
                  @if(isset($roles) && isset($roles[$role])) active @endif
                ">
                    <input type="checkbox" name="roles[{{ $role }}]" autocomplete="off"
                    @if(isset($roles) && isset($roles[$role])) checked @endif
                    > {{ $label }}
                </label>
              @endforeach
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Miejsce pracy:</strong>
                <select name="work_id">
                  <option></option>
                  @foreach($works as $work)
                      <option value="{{ $work->id }}">{{ $work->name }}</option>
                  @endforeach
                </select>
                <select name="section_id">
                  <option></option>
                  </select>
              </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Hasło:</strong>
                <input class="form-control" name="password" type="password" placeholder="Podaj hasło"/>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Powtórz:</strong>
                <input class="form-control" name="password2" type="password" placeholder="Powtórz hasło"/>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Zapisz</button>
        </div>
    </div>
    <script type="application/javascript">
    $(document).ready(function(){
      $("[name=work_id]").change(function(){
        var $sections = $("[name=section_id]");
        $sections.find('[value]').remove();
        if($(this).val()!="") {
          $.post("{{ route('works.sections') }}", {"_token": "{{ csrf_token() }}", work: $(this).val()}, function(data){

            $.each(data.sections, function(i, item) {
              var $option = $('<option/>');
              $option.attr('value', item.id);
              $option.text(item.name);
              $sections.append($option);
            });
          });
        }
      });
    });

    </script>
</form>



@endsection
