@extends('layouts.app')
@section('content')

<h1>Lista użytkowników</h1>
@include('searcher', array('route' => 'users.index', 'search' => $search))
<div class="row">
  <div class="col-lg-12 actions">
    <div class="form-group">
      <a class="modal-action btn btn-primary" href="{{ route('users.create') }}">Dodaj</a>
    </div>
  </div>

<table class="table table-bordered table-responsive-lg">
       <tr>
           <th>Imię i nazwisko</th>
           <th>Email</th>
           <th>Miejsce</th>
           <th>Role</th>
           <th>Akcje</th>

       </tr>
       @foreach ($users as $user)
           <tr>
               <td>{{ $user->name }}</td>
               <td>{{ $user->email }}</td>
               <td>
                 @if(isset($user->work))
                  {{ $user->work->name }}
                    @if(isset($user->section))
                    / {{ $user->section->name }}
                    @endif
                 @endif
               </td>
               <td>
                 @foreach(array($user::ROLE_SECURITY => 'ochroniarz', $user::ROLE_ADMIN => 'administrator') as $role => $label)
                     @if($user->roles & $role) {{ $label }} @endif
                 @endforeach
               </td>

               <td>
                 <a class="modal-action btn btn-primary" href="{{ route('users.edit',$user->id) }}">Edytuj</a>
                 <form action="{{ route('users.destroy',$user->id) }}" method="POST">
                     @csrf
                     @method('DELETE')
                     <button type="submit" class="btn btn-danger">Usuń</button>
                 </form>
               </td>
           </tr>
       @endforeach
   </table>

</div>

@include('dialog')

@endsection
