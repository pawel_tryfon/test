<form method="GET" action="{{ route($route) }}" class="row" id="searcher">
    <div class="col-xs-12 col-sm-12 col-md-4">
      <div class="form-group">
        <input type="text" name="search" value="{{ isset($search['search'])?$search['search']:'' }}"/>
      </div>
    </div>
    @if(Auth::user()->isAdmin())
      <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="form-group">
            <strong>Miejsce pracy:</strong>
            <select name="work">
              <option></option>
              @foreach(App\Models\Work::all() as $work)
                  <option value="{{ $work->id }}"
                    @if(isset($search['work']) && $search['work'] == $work->id)
                      selected = "selected"
                      <?php $sections = $work->sections;?>
                    @endif
                    >{{ $work->name }}</option>
              @endforeach
            </select>
            <select name="section">
              <option></option>
                @if(isset($sections))
                  @foreach($sections as $section)
                      <option value="{{ $section->id }}"
                        @if(isset($search['section']) && $search['section'] == $section->id)
                          selected = "selected"
                        @endif
                        >{{ $section->name }}</option>
                  @endforeach
                @endif
              </select>
          </div>
        </div>
      @endif
      <div class="col-xs-12 col-sm-2">
        <div class="form-group">
          <button type="submit" class="btn btn-primary float-right">Szukaj</button>
      </div>
    </div>
</form>
<script type="application/javascript">
$(document).ready(function(){
  $("[name=work]").change(function(){
    var $sections = $("[name=section]");
    $sections.find('[value]').remove();
    if($(this).val()!="") {
      $.post("{{ route('works.sections') }}", {"_token": "{{ csrf_token() }}", work: $(this).val()}, function(data){

        $.each(data.sections, function(i, item) {
          var $option = $('<option/>');
          $option.attr('value', item.id);
          $option.text(item.name);
          $sections.append($option);
        });
      });
    }
  });
});

</script>
