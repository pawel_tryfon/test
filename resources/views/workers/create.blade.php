@extends('layouts.app')
@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Nowy pracownik</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('users.index') }}"> Wróć</a>
        </div>
    </div>
</div>

@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Uwaga!</strong> Wystąpiły błędy.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{ route('workers.store') }}" method="POST">
    @csrf

     <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Imię:</strong>
                <input type="text" name="name" value="{{ old('name') }}" class="form-control" placeholder="Imię">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Nazwisko:</strong>
                <input type="text" name="surname" value="{{ old('surname') }}" class="form-control" placeholder="Nazwisko">
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Miejsce pracy:</strong>
                <select name="work_id">
                  <option></option>
                  @foreach($works as $work)
                      <option value="{{ $work->id }}">{{ $work->name }}</option>
                  @endforeach
                </select>
                <select name="section_id">
                  <option></option>
                  </select>
              </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
          <button type="submit" class="btn btn-primary">Zapisz</button>
        </div>
    </div>
    <script type="application/javascript">
    $(document).ready(function(){
      $("[name=work_id]").change(function(){
        var $sections = $("[name=section_id]");
        $sections.find('[value]').remove();
        if($(this).val()!="") {
          $.post("{{ route('works.sections') }}", {"_token": "{{ csrf_token() }}", work: $(this).val()}, function(data){

            $.each(data.sections, function(i, item) {
              var $option = $('<option/>');
              $option.attr('value', item.id);
              $option.text(item.name);
              $sections.append($option);
            });
          });
        }
      });
    });

    </script>
</form>




@endsection
