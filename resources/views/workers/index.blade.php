@extends('layouts.app')
@section('content')

<h1>Lista pracowników</h1>
@include('searcher', array('route' => 'workers.index', 'search' => $search))
<div class="row">
  <div class="col-lg-12 actions">
    <div class="form-group">
      <a class="modal-action btn btn-primary" href="{{ route('workers.create') }}">Dodaj</a>
      <a class="btn btn-primary" href="{{ route('workers_logs.export', $search) }}">Raport</a>
    </div>
  </div>
<table class="table table-bordered table-responsive-lg">
       <tr>
           <th>Imię i nazwisko</th>
           <th>Praca</th>
           <th>Akcje</th>

       </tr>
       @foreach ($workers as $worker)
           <tr>
               <td>{{ $worker->name }} {{ $worker->surname }}</td>
               <td>
                 @if(isset($worker->work))
                  {{ $worker->work->name }}
                    @if(isset($worker->section))
                    / {{ $worker->section->name }}
                    @endif
                 @endif
               </td>
               <td>
                 @if(isset($worker->work) && isset($worker->section))
                 <a class="btn btn-primary" href="{{ route('workers_logs.index',['worker'=>$worker->id]) }}">Wejścia/wyjścia</a>
                 @endif
                 <a class="modal-action btn btn-primary" href="{{ route('workers.edit',$worker->id) }}">Edytuj</a>
                 <form action="{{ route('workers.destroy',$worker->id) }}" method="POST">
                     @csrf
                     @method('DELETE')
                     <button type="submit" class="btn btn-danger">Usuń</button>
                 </form>
               </td>
           </tr>
       @endforeach
   </table>

   {!! $workers->links() !!}

   @include('dialog')

@endsection
