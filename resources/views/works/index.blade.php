@extends('layouts.app')
@section('content')

<h1>Lista zakładów</h1>
<div class="row">
  <div class="col-lg-12 actions">
    <div class="form-group">
      <a class="modal-action btn btn-primary" href="{{ route('works.create') }}">Dodaj</a>
      <a class="btn btn-primary" href="{{ route('workers_logs.export') }}">Raport</a>
    </div>
  </div>
<table class="table table-bordered table-responsive-lg">
       <tr>
           <th>Nazwa</th>
           <th>Akcje</th>

       </tr>
       @foreach ($works as $work)
           <tr>
               <td>{{ $work->name }}</td>
               <td>
                 <a class="btn btn-primary" href="{{ route('workers_logs.export', ['work'=>$work->id]) }}">Wejścia/wyjścia</a>
                 <a class="btn btn-primary" href="{{ route('sections.index',['work'=>$work->id]) }}">Działy</a>
                 <a class="modal-action btn btn-primary" href="{{ route('works.edit',$work->id) }}">Edytuj</a>

                 <form action="{{ route('works.destroy',$work->id) }}" method="POST">
                     @csrf
                     @method('DELETE')
                     <button type="submit" class="btn btn-danger">Usuń</button>
                 </form>
               </td>
           </tr>
       @endforeach
   </table>

   {!! $works->links() !!}

   @include('dialog')

@endsection
