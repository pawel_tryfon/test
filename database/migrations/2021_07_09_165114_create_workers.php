<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workers', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('surname');
            $table->foreignId('work_id')
              ->nullable()
              ->constrained('works')->nullOnDelete();;
            $table->foreignId('section_id')
                ->nullable()
                ->constrained('sections')->nullOnDelete();;

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('workers', function($table) {
        $table->dropForeign(['work_id']);
        $table->dropForeign(['section_id']);

      });
      Schema::dropIfExists('workers');

    }
}
