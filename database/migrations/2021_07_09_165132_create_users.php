<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->tinyInteger('roles')->default(0);
            $table->bigInteger('work_id')
              ->unsigned()
              ->nullable();
            $table->foreign('work_id')
              ->references('id')
              ->on('works')->nullOnDelete();;
            $table->bigInteger('section_id')
              ->unsigned()
              ->nullable();
            $table->foreign('section_id')
              ->references('id')
              ->on('sections')->nullOnDelete();;


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('users', function($table) {
        $table->dropForeign(['work_id']);
        $table->dropForeign(['section_id']);
      });

    }

}
