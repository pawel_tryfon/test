<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWokersLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('workers_logs', function (Blueprint $table) {
            $table->id();
            $table->datetime('start')->nullable();
            $table->datetime('stop')->nullable();
            $table->foreignId('work_id')
               ->nullable()
               ->constrained('works')->nullOnDelete();
            $table->foreignId('section_id')
                ->nullable()
                ->constrained('sections')->nullOnDelete();
            $table->foreignId('worker_id')
                ->nullable()
                ->constrained('workers')->nullOnDelete();
            $table->foreignId('user_id')
              ->nullable()
              ->constrained('users')->nullOnDelete();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('workers_logs');
    }
}
