<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSections extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sections', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->bigInteger('work_id')
              ->unsigned()
              ->nullable();
            $table->foreign('work_id')
              ->references('id')
              ->on('works')->nullOnDelete();;
              // ->constrained('works');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('sections', function($table) {
        $table->dropForeign(['work_id']);
      });

      Schema::dropIfExists('sections');
    }

}
