<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
      $this->call(UserTableSeeder::class);
      $this->call(WorkTableSeeder::class);
      $this->call(SectionTableSeeder::class);
      $this->call(WorkerTableSeeder::class);
      $this->call(WorkerLogTableSeeder::class);

    }
}
