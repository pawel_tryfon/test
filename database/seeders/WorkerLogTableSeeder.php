<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class WorkerLogTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\WorkerLog::factory(40)->create();
    }
}
