<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use \App\Models\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $admin = User::where('name', 'admin')->first();
      if(!$admin) {
        User::factory(User::class)->create([
          'name' => 'admin',
          'email' => 'admin@admin.com',
          'password' => bcrypt('admin'),
          'roles' => 1
        ]);
      }
        \App\Models\User::factory(10)->create();
    }
}
