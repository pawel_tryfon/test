<?php

namespace Database\Factories;

use App\Models\Worker;
use App\Models\Work;
use App\Models\Section;
use Illuminate\Database\Eloquent\Factories\Factory;

class WorkerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Worker::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
      $sectionId = Section::pluck('id')->random();
      $section = Section::find($sectionId);
      return [
          'name' => $this->faker->firstName(),
          'surname' => $this->faker->lastName(),
          'work_id' => $section->work_id,
          'section_id' => $section->id
      ];
    }
}
