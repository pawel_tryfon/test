<?php

namespace Database\Factories;

use App\Models\WorkerLog;
use App\Models\Worker;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class WorkerLogFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = WorkerLog::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
      $userId = User::pluck('id')->random();
      $workerId = Worker::pluck('id')->random();
      $worker = Worker::find($workerId);
      $start = $this->faker->dateTimeThisMonth()->format('Y-m-d H:i:s');
      $minutes = \rand(1, 600);
      $stop = date('Y-m-d H:i:s', strtotime($start.' +'.$minutes.'minutes'));


      return [
          'start' => $start,
          'stop' => $stop,
          'work_id' => $worker->work_id,
          'section_id' => $worker->section->id,
          'worker_id' => $worker->id,
          'user_id' => $userId,
      ];
    }
}
